// Membuat permintaan HTTP menggunakan fetch API
fetch('https://reqres.in/api/users?page=2')
  .then(response => response.json())
  .then(data => {
    // Menampilkan data pengguna di dalam elemen dengan id "produk-container"
    const users = data.data;
    const produkContainer = document.getElementById('produk-container');
    users.forEach(user => {
      const cardDiv = document.createElement('div');
      cardDiv.classList.add('col-lg-4');
      cardDiv.innerHTML = `
        <div class="card" style="width: 300px;">
          <img src="${user.avatar}" class="card-img-top" alt="${user.first_name} ${user.last_name}">
          <div class="card-body">
            <h5 class="card-title">${user.first_name} ${user.last_name}</h5>
            <button type="button" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#exampleModal" data-user-id="${user.id}">
              Cek Email
            </button>
          </div>
        </div>`;
      produkContainer.appendChild(cardDiv);
    });

    // Tambahkan event listener untuk menangani klik tombol "Detail Data"
    const detailButtons = document.querySelectorAll('[data-bs-target="#exampleModal"]');
    detailButtons.forEach(button => {
      button.addEventListener('click', function() {
        const userId = this.getAttribute('data-user-id');
        const user = users.find(user => user.id == userId);
        const modalTitle = document.querySelector('.modal-title');
        const modalBody = document.querySelector('.modal-body');
        modalTitle.textContent = `${user.first_name} ${user.last_name}`;
        modalBody.querySelector('img').src = user.avatar;
        modalBody.querySelector('p').textContent = `Email: ${user.email}`;
      });
    });
  })
  .catch(error => console.error('Terjadi kesalahan:', error));


// Membuat permintaan HTTP menggunakan fetch API
fetch('https://reqres.in/api/users?page=2')
  .then(response => response.json())
  .then(data => {
    // Menampilkan data pengguna sebagai testimonial
    const users = data.data;
    const testimonialsContainer = document.getElementById('testimonials');
    users.forEach(user => {
      const testimonialBlock = document.createElement('blockquote');
      testimonialBlock.classList.add('col-4', 'testimonial', 'classic');
      testimonialBlock.innerHTML = `
        <img src="${user.avatar}" alt="User"/>
        <q>${user.email}</q>
        <footer>${user.first_name} ${user.last_name}</footer>`;
      testimonialsContainer.appendChild(testimonialBlock);
    });
  })
  .catch(error => console.error('Terjadi kesalahan:', error));
