<?php
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Login</title>
    <link rel="stylesheet" href="assets/Css/main.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,100..900;1,100..900&display=swap"
      rel="stylesheet"
    />
  </head>
  <body>
    <div class="container">
      <h1>Login</h1>
      <p>Silahkan login untuk melanjutkan</p>
      <form action="http://localhost/belajar-php/List-produk.php">
        <div class="form-group">
          <label for="username">Username</label>
          <div class="input-with-icon">
            <input
              type="text"
              id="username"
              name="username"
              placeholder="Username"
              required
            />
            <img src="assets/images/email.png" class="icon" alt="Surat Icon" />
          </div>
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <div class="input-with-icon">
            <input
              type="password"
              id="password"
              name="password"
              placeholder="Password"
              required
            />
            <img src="assets/images/padlock.png" class="icon" alt="Gembok Icon" />
          </div>
        </div>
        <div class="form-group">
          <div class="remember-login">
            <input type="checkbox" id="remember" name="remember" />
            <label for="remember"> Remember Me</label>
          </div>
          <button type="submit" class="login-btn">Login</button>
        </div>
        <div class="form-group">
          <a href="#">Lupa password</a>
        </div>
        <div class="form-group">
          <a href="http://localhost/belajar-php/List-produk.php" class="register-btn">Register</a>
        </div>
      </form>
    </div>
  </body>
</html>
